const usersData = require('../data/usersData');

const saveData = (data) => {
    if (data) {
        console.log(`${data} is saved`);    
        return "saved";    
    } else {
        return null;
    }
}

const getDataById = (id) => {
    return usersData.find((element) => {
        return element._id === id;
    });
}

const getUsers = () => {
    return usersData;
}

module.exports = {
    saveData,
    getUsers,
    getDataById
}