const { saveData } = require("../repositories/user.repository")
//Этим файлом мы возвращаем имя пользователя, если оно есть
const getName = (user) => {
    if (user) {
        return user.name;
    } else {
        return null;
    }
}

const saveName = (user) => {
    if (user) {
        return saveData(user.name);
    } else {
        return null;
    }
}

//Команда для экспорта модуля в другие js файлы
module.exports = {
    getName,
    saveName
}