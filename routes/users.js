var express = require('express');
const userRepo = require('../repositories/user.repository');
var router = express.Router();
//Забираем данные getName
const { getName } = require("../services/user.service");
const { saveName } = require("../services/user.service");
const { isAuthorized } = require("../middlewares/auth.middleware");

router.get('/', function(req, res, next) {
  const users = userRepo.getUsers();
  res.send(users);
});

//        
router.get('/:id', (req, res) => {
  const user = userRepo.getDataById(req.params.id)
  res.send(user);
});


//---------------------------
router.post('/', isAuthorized, function(req, res, next) {
  // const get = getName(req.body);
  const result = saveName(req.body);
  if (result){
    res.send(`The name is ${result}`);
  } else {
    res.status(400).send(`Here is some error`)
  }
});
//---------------------------


module.exports = router;
